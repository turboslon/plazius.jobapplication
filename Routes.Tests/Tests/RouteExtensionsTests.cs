﻿using NUnit.Framework;

namespace Routes.Tests
{
    [TestFixture]
    public class RouteExtensionsTests
    {
        private const string DefaultString = "bla-bla";
        private const int DefaultWidth = 6;
        
        [TestCase(null, ExpectedResult = RouteExtensions.EmptyString)]
        [TestCase("", ExpectedResult = RouteExtensions.EmptyString)]
        [TestCase("a", ExpectedResult = "a")]
        [TestCase("aaaaa", ExpectedResult = "aaaaa")]
        [TestCase("aaaaaa", ExpectedResult = "aaaaaa")]
        [TestCase("aaaaaaa", ExpectedResult = "...aaa")]
        [TestCase("aaaaaaaaaa", ExpectedResult = "...aaa")]
        public string ToDebugStringTest(string text)
        {
            var result = RouteExtensions.ShortenText(text, DefaultWidth);
            Assert.LessOrEqual(result.Length, DefaultWidth);
            return result;
        }
    }
}