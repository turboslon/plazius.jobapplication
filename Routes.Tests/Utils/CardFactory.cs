﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Routes.Tests
{
    /// <summary>
    /// Test helper: Generator of Route cards
    /// </summary>
    internal class CardFactory
    {
        private Random Rnd { get; }

        public CardFactory(Random rnd = null)
        {
            Rnd = rnd ?? new Random();
        }

        /// <summary>
        /// Generate list of cards with one head and one tail without cycles
        /// </summary>
        /// <param name="count">required cards amount</param>
        /// <param name="shuffle">do the shuffling?</param>
        /// <returns>list of cards with one head and one tail without cycles</returns>
        /// <exception cref="ArgumentException">If <c>count</c> is less or equal to zero</exception>
        public IList<Route> Get(int count, bool shuffle = true)
        {
            if (count <= 0) throw new ArgumentException($"{nameof(count)} expected to be greater than 0");
            List<Route> result = new List<Route>();
            string lastCity = null;
            for (int i = 0; i < count; i++)
            {
                result.Add(BuildNewCard(ref lastCity));
            }
            if (shuffle) result = result.OrderBy(x => Rnd.Next()).ToList();
            
            return result;
        }

        private Route BuildNewCard(ref string lastCity)
        {
            if (lastCity == null) lastCity = GetNewCity();
            var newCity = GetNewCity();
            var card = new Route(lastCity, newCity);
            lastCity = newCity;
            return card;
        }
        private string GetNewCity()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}