﻿using System;

namespace Routes.Tests
{
    /// <summary>
    /// Test Helper: Extensions to Route class
    /// </summary>
    public static class RouteExtensions
    {
        /// <summary>
        /// Minimal printable width
        /// </summary>
        public const int MinimalWidth = 6;
        public const string EmptyString = "(None)";
        private const string SkipStringPart = "...";

        /// <summary>
        /// Get string like "city from -> city to"
        /// </summary>
        /// <param name="route">Route object to make string</param>
        /// <param name="width">City name maximum width</param>
        /// <returns></returns>
        /// <exception cref="NullReferenceException">If route is null</exception>
        /// <exception cref="ArgumentException">if width is less or equal to <see cref="MinimalWidth"/></exception>
        public static string ToDebugString(this Route route, int width)
        {
            if (route == null) throw new NullReferenceException(nameof(route));
            if (width < MinimalWidth)
                throw new ArgumentException(
                    $"{nameof(width)} expected to be greater than {MinimalWidth}, but {width} found");
            var cityFrom = ShortenText(route.From, width);
            var cityTo = ShortenText(route.To, width);
            return $"{cityFrom} -> {cityTo}";
        }

        internal static string ShortenText(string text, int width)
        {
            if (string.IsNullOrEmpty(text))
                return EmptyString;
            if (text.Length <= width)
                return text;
            var cut = text.Substring(Math.Max(0, text.Length - (width - SkipStringPart.Length)));
            return $"{SkipStringPart}{cut}";
        }
    }
}