﻿using System;

namespace Routes
{
    /// <summary>
    /// Route object describing route between two cities
    /// </summary>
    public class Route
    {
        /// <summary>
        /// City to start from
        /// </summary>
        public string From { get; }
        /// <summary>
        /// City to head to
        /// </summary>
        public string To { get; }

        /// <summary>
        /// Create new Route object describing route between two cities
        /// </summary>
        /// <param name="from">City to start route from</param>
        /// <param name="to">City to head to</param>
        /// <exception cref="ArgumentException">If <c>from</c> or <c>to</c> city is not set</exception>
        public Route(string from, string to)
        {
            if (string.IsNullOrEmpty(from)) throw new ArgumentException(nameof(from));
            if (string.IsNullOrEmpty(to)) throw new ArgumentException(nameof(to));

            From = from;
            To = to;
        }
    }
}
