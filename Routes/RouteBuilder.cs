﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Routes
{
    /// <summary>
    /// Route Builder class.
    /// </summary>
    public class RouteBuilder
    {
        private readonly ReaderWriterLockSlim _lock = new ReaderWriterLockSlim();
        /// <summary>
        /// Builds route from insorted list of <see cref="Route"/>
        /// Prerequirements:
        /// * There's only one city to start from
        /// * There's only one city to finish at
        /// * There's no cycles in routes
        /// </summary>
        /// <param name="routeList">Unsorted list of Route objects</param>
        /// <returns></returns>
        public IList<Route> BuildRoute(IList<Route> routeList)
        {
            Validate(routeList);
            
            var linkedList = new LinkedList<Route>();
            Task[] tasks = {
                Task.Factory.StartNew(() => BuildForwardPath(routeList, linkedList)),
                Task.Factory.StartNew(() => BuildBackwardsPath(routeList, linkedList))
            };
            Task.WaitAll(tasks);
            return linkedList.ToList();
        }

        private void BuildBackwardsPath(IList<Route> routeList, LinkedList<Route> linkedList)
        {
            var byTo = routeList.ToDictionary(x => x.To, x => x);
            var next = routeList[0];
            while (next != null)
            {
                next = byTo.ContainsKey(next.From)
                    ? byTo[next.From]
                    : null;

                if (next != null)
                {
                    _lock.EnterWriteLock();
                    try
                    {
                        linkedList.AddFirst(next);
                    }
                    finally
                    {
                        _lock.ExitWriteLock();
                    }
                }
            }
        }

        private void BuildForwardPath(IList<Route> routeList, LinkedList<Route> linkedList)
        {
            var byFrom = routeList.ToDictionary(x => x.From, x => x);
            var next = routeList[0];
            while (next != null)
            {
                _lock.EnterWriteLock();
                try
                {
                    linkedList.AddLast(next);
                }
                finally
                {
                    _lock.ExitWriteLock();
                }
                next = byFrom.ContainsKey(next.To)
                    ? byFrom[next.To]
                    : null;
            }
        }

        private void Validate(IList<Route> routeList)
        {
            if (routeList == null) throw new ArgumentNullException(nameof(routeList));
            if (routeList.Count == 0) throw new ArgumentException($"Route list should contain at least one route");
        }
    }
}