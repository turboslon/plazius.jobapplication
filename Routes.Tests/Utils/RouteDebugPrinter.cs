﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Routes.Tests
{
    /// <summary>
    /// Test Helper: Printer for cards
    /// Prints both sorted and shuffled route lists as two-column table
    /// </summary>
    public class RouteDebugPrinter
    {
        private int Width { get; }

        /// <summary>
        /// Create Route cards printer
        /// </summary>
        /// <param name="width">Maximum City name width</param>
        /// <exception cref="ArgumentException">If width is less or equal to <see cref="RouteExtensions.MinimalWidth"/></exception>
        public RouteDebugPrinter(int width)
        {
            if (width < RouteExtensions.MinimalWidth)
                throw new ArgumentException(
                    $"{nameof(width)} expected to be greater or equal to {RouteExtensions.MinimalWidth}");
            Width = width;
        }

        /// <summary>
        /// Prints both sorted and shuffled route lists as two-column table
        /// </summary>
        /// <param name="unsorted"></param>
        /// <param name="sorted"></param>
        public void PrintBoth(IList<Route> unsorted, IList<Route> sorted)
        {
            Console.WriteLine("Before sorting:\t\tAfter sorting:");
            Console.WriteLine(new string('-', Width * 4 + 11));
            for (int i = 0; i < unsorted.Count; i++)
            {
                var route1 = unsorted[i].ToDebugString(Width);
                var route2 = sorted[i].ToDebugString(Width);
                var line = $"{route1} | {route2}";
                Console.WriteLine(line);
            }
        }
    }
}