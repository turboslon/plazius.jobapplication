using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Routes.Tests
{
    /// <summary>
    /// Tests required by job application
    /// </summary>
    [TestFixture]
    public class RouteBuilderTests
    {
        // private const int DefaultSeed = 16384;
        private CardFactory _factory;
        private Random _rnd;

        [SetUp]
        public void TestSetup()
        {
            _rnd = new Random(); // (DefaultSeed);
            _factory = new CardFactory(_rnd);
        }
        
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(10)]
        [TestCase(1000)]
        public void BuildsCorrectRoute(int cardsCount)
        {
            var cards = _factory.Get(cardsCount, false);
            var shuffled = cards.OrderBy(x => _rnd.Next()).ToList();
            var routeBuilder = new RouteBuilder();
            var sorted = routeBuilder.BuildRoute(shuffled);
            Assert.AreEqual(cards.Count, sorted.Count);
            for (int i=0;i<sorted.Count;i++)
            {
                Assert.AreSame(cards[i], sorted[i]);
            }

            var printer = new RouteDebugPrinter(7);
            printer.PrintBoth(shuffled, sorted);
        }

        [Test]
        public void ThrowsOnEmptyList()
        {
            var routeBuilder = new RouteBuilder();
            Assert.Throws<ArgumentException>(() => routeBuilder.BuildRoute(new List<Route>()));
        }
        
        [Test]
        public void ThrowsOnNullList()
        {
            var routeBuilder = new RouteBuilder();
            Assert.Throws<ArgumentNullException>(() => routeBuilder.BuildRoute(null));
        }
    }
}